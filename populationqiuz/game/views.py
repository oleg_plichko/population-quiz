#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
import urllib2
import json
from django.http.response import JsonResponse
from django.views.generic.base import View
from .models import City



GEONAMES_URL = 'http://api.geonames.org/searchJSON?username=oleg_plichko&name={name}&maxRows=1&lang=ru'


def calculate_accuracy(guess, value):
    return int((1-abs(value-guess)/max(value, guess))*100)


class MakeAGuessView(View):

    def post(self, request, *args, **kwargs):
        name = request.POST.get('name')
        population = request.POST.get('population')
        name = name.encode('utf-8').strip()
        try:
            city = City.objects.get(name=name)
        except City.DoesNotExist:
            url = GEONAMES_URL.format(name=name)
            response = urllib2.urlopen(url)
            response = json.load(response)['geonames'][0]
            city = City(
                name=response['name'],
                population=response['population']
            )
            city.save()

        population = int(population)
        accuracy = calculate_accuracy(population, city.population)
        data = {
            'guess':
                {
                    'name': name,
                    'guess': population,
                    'population': city.population,
                    'accuracy': accuracy
                }
        }

        average_accuracy = int((request.session.get('average_accuracy', 100) + accuracy) * 0.5)
        request.session['average_accuracy'] = average_accuracy
        data['average_accuracy'] = average_accuracy
        return JsonResponse(data)


def reset(request):
    request.session['average_accuracy'] = 100
    return JsonResponse({'message': 'Сброс результатов'})





