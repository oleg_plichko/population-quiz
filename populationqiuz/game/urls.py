from django.conf.urls import url
from django.views.generic.base import TemplateView
from .views import MakeAGuessView, reset


urlpatterns = [
    url(r'guess/$', MakeAGuessView.as_view(), name='make_a_guess'),
    url(r'reset/$', reset, name='reset'),
    url(r'$', TemplateView.as_view(template_name="game/index.html"), name='index'),
]
